package pl.magg;

import junit.framework.Assert;
import junit.framework.TestCase;
import lombok.val;
import pl.magg.entity.Application;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

	public void setUp() {
		System.out.println("SETUP");

	}

	public void testAddApplications() {
		Manager man = new Manager();
		man.CreateApplication("atest1", "test1c");
		man.CreateApplication("atest2", "test2c");

		val list = man.listApplications("atest1", 0, null, 0);
		Assert.assertEquals("atest1", list.get(0).getName());
		Assert.assertEquals(1, list.size());
	}

	public void testSearch() {
		Manager man = new Manager();
		val o1 = man.CreateApplication("btest1", "test1c");
		val o2 = man.CreateApplication("btest2", "test2c");
		val o3 = man.CreateApplication("btest3", "test3c");

		o1.setState(Application.stateVerified, null);
		man.updateApplication(o1);

		o2.setState(Application.stateVerified, null);
		o2.setState(Application.stateAccepted, null);
		man.updateApplication(o2);

		o3.setState(Application.stateVerified, null);
		o3.setState(Application.stateAccepted, null);
		o3.setState(Application.statePublished, null);
		man.updateApplication(o3);

		Assert.assertNotNull(o3.getPublicationNumber());
		val list = man.listApplications("btest", 0, null, 0);
		Assert.assertEquals(3, list.size());
		val list2 = man.listApplications("", Application.stateAccepted, null, 0);
		Assert.assertTrue(1 <= list2.size());
		val list3 = man.listApplications("", 0, o3.getId(), 0);
		Assert.assertEquals(1, list3.size());
	}

	public void testSearch10() {
		Manager man = new Manager();

		for (int i = 1; i <= 25; i++) {
			man.CreateApplication("ctest" + i, "test" + i + "c");
		}

		val list = man.listApplications("ctest", 0, null, 0);
		Assert.assertEquals(10, list.size());
		val list2 = man.listApplications("ctest", 0, null, 10);
		Assert.assertEquals(10, list2.size());
		val list3 = man.listApplications("ctest", 0, null, 20);
		Assert.assertEquals(5, list3.size());
	}

	public void testStateNames() {
		String t1 = Application.getStateName(0);
		Assert.assertEquals(t1, "UNDEFINED");

		String t2 = Application.getStateName(1);
		Assert.assertEquals(t2, "CREATED");
	}

	public void testStateBlocks() {
		Manager man = new Manager();
		val o1 = man.CreateApplication("dtest1", "test1c");
		o1.setState(Application.stateAccepted, "");
		Assert.assertEquals(Application.stateCreated, o1.getState());
		o1.setState(Application.statePublished, "");
		Assert.assertEquals(Application.stateCreated, o1.getState());
		o1.setState(Application.stateDeleted, "");
		Assert.assertEquals(Application.stateCreated, o1.getState());
		o1.setState(Application.stateDeleted, "delete");
		Assert.assertEquals(Application.stateDeleted, o1.getState());
		Assert.assertEquals("delete", o1.getReason());
	}

	public void testStateBlocks2() {
		Manager man = new Manager();
		val o1 = man.CreateApplication("etest1", "test1c");
		o1.setState(Application.stateVerified, "");
		Assert.assertEquals(Application.stateVerified, o1.getState());
		o1.setState(0, "");
		Assert.assertEquals(Application.stateVerified, o1.getState());
		o1.setState(Application.statePublished, "");
		Assert.assertEquals(Application.stateVerified, o1.getState());
		o1.setState(Application.stateDeleted, "delete");
		Assert.assertEquals(Application.stateVerified, o1.getState());
		o1.setState(Application.stateRejected, "");
		Assert.assertEquals(Application.stateVerified, o1.getState());
		o1.setState(Application.stateRejected, "rej");
		Assert.assertEquals(Application.stateRejected, o1.getState());
	}

	public void testContenet() {
		Manager man = new Manager();
		val o1 = man.CreateApplication("ftest1", "test1c");
		Assert.assertTrue(o1.setContent("cont3a"));
		o1.setState(Application.stateVerified, "");
		Assert.assertTrue(o1.setContent("cont3b"));
		o1.setState(Application.stateAccepted, "");
		Assert.assertFalse(o1.setContent("cont3c"));
		Assert.assertEquals("cont3b", o1.getContent());
	}

	public void testHistory() {
		Manager man = new Manager();
		val o1 = man.CreateApplication("gtest1", "test1c");
		o1.setState(Application.stateVerified, "");
		man.updateApplication(o1);
		o1.setState(Application.stateAccepted, "");
		man.updateApplication(o1);

		val list = man.getHistory(o1.getId());
		Assert.assertEquals(3, list.size());
		Assert.assertEquals(Application.stateVerified, list.get(1).getState());
		Assert.assertNotNull(list.get(2).getTimestamp());
		Assert.assertEquals(o1.getId(), list.get(2).getApplicationId());
		Assert.assertNotNull(list.get(2).getId());

	}

}