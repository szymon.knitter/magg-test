package pl.magg;

/**
 * Just start UI
 *
 */
public class App {
	public static void main(String[] args) {
		System.out.println("Program Start!");
		AppUI ui = new AppUI();
		ui.runUI();
		System.out.println("Program End!");
	}
}