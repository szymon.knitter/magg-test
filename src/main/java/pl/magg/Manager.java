package pl.magg;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import pl.magg.entity.Application;
import pl.magg.entity.ApplicationHistory;

/**
 * Manager class, to get access to objects
 *
 */
public class Manager {
	private SessionFactory sessionFactory;
	private ServiceRegistry serviceRegistry;

	private SessionFactory configureSessionFactory() throws HibernateException {
		Configuration configuration = new Configuration();
		configuration.configure();
		serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		return sessionFactory;
	}

	private SessionFactory getSessionFactory() {
		return configureSessionFactory();
	}

	public Application CreateApplication(String name, String content) {
		Session session = getSessionFactory().openSession();
		session.beginTransaction();

		Application app = new Application(name, content);
		session.save(app);
		ApplicationHistory appHist = new ApplicationHistory(app.getId(), app.getState());
		session.save(appHist);
		session.getTransaction().commit();
		session.close();
		return app;
	}

	public void updateApplication(Application app) {
		Session session = getSessionFactory().openSession();
		session.beginTransaction();
		session.update(app);
		ApplicationHistory appHist = new ApplicationHistory(app.getId(), app.getState());
		session.save(appHist);
		session.getTransaction().commit();
		session.close();
	}

	public List<ApplicationHistory> getHistory(Long id) {
		List<ApplicationHistory> list = new ArrayList<ApplicationHistory>();
		Session session = getSessionFactory().openSession();
		session.beginTransaction();

		Criteria criteria = session.createCriteria(ApplicationHistory.class);
		criteria.add(Restrictions.eq("applicationId", id));
		for (Object o : criteria.list()) {
			list.add((ApplicationHistory) o);
		}
		session.getTransaction().commit();
		session.close();
		return list;
	}

	public List<Application> listApplications(String name, int state, Long id, int startAt) {
		List<Application> list = new ArrayList<Application>();
		Session session = getSessionFactory().openSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(Application.class);
		if (state > 0) {
			criteria.add(Restrictions.eq("state", state));
		}
		if (id != null) {
			criteria.add(Restrictions.idEq(id));
		}
		if (!name.isEmpty()) {
			criteria.add(Restrictions.like("name", "%" + name + "%"));
		}
		criteria.setFirstResult(startAt);
		criteria.setMaxResults(10);
		for (Object o : criteria.list()) {
			list.add((Application) o);
		}
		session.getTransaction().commit();
		session.close();
		return list;
	}
}
