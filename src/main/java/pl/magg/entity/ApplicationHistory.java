package pl.magg.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;

@Entity

public class ApplicationHistory implements Serializable {

	private static final long serialVersionUID = -5342348747230267775L;

	public ApplicationHistory() {

	}

	public ApplicationHistory(Long applicationId, int state) {
		this.applicationId = applicationId;
		this.state = state;
		this.timestamp = new Date();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	private Long id;
	@Getter
	private Long applicationId;
	@Getter
	private int state;
	@Getter
	private Date timestamp;
}