package pl.magg.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
 
@Entity

public class Application implements Serializable {
 
	private static final long serialVersionUID = -5973638747230267775L;
	
	public final static int   stateCreated 	= 1;
	public final static int   stateVerified 	= 2;
	public final static int   stateAccepted 	= 3;
	public final static int   statePublished = 4;
	public final static int   stateRejected 	= 5;
	public final static int   stateDeleted 	= 6;

	public static String getStateName(int state) {
		switch (state) {
			case stateCreated: 	return "CREATED";
			case stateVerified: 	return "VERIFIED";
			case stateAccepted: 	return "ACCEPTED";
			case statePublished: 	return "PUBLISHED";
			case stateRejected: 	return "REJECTED";
			case stateDeleted: 	return "DELETED";
		}
		return "UNDEFINED";
	}
	
	public Application() {
 
	};
 
	public Application(String name, String content) {
		this.name = name;
		this.content = content;
		this.state = stateCreated;
	};
 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter private Long id;
 
	@Getter private String name;
	@Getter 		private String content;
	@Getter private int state;
	@Getter private String reason;
	@Getter private Long publicationNumber;
	
	/**
	 * Set content if possible
	 * @param content
	 * @return true if content was changed, false if it was forbidden
	 */
	public boolean setContent(String content) {
		if (this.state == stateCreated || this.state == stateVerified) {
			this.content = content;
			return true;
		}		
		return false;
	}
	
	private void setAutoPublicationNumber() {
		// this might be unique and numerated from 1 without gaps (then it should be get from database stored procedure or sequence)
		this.publicationNumber = this.id;
	}
	
	/**
	 * Set state if possible
	 * @param state
	 * @param reason
	 * @return true if set
	 */
	public boolean setState(int state, String reason) {
		// check current state
		switch (state) {
			case stateVerified:
			case stateDeleted:
				if (this.state != stateCreated)
					return false;
				break;
			case stateRejected:
			case stateAccepted:
				if (this.state != stateVerified)
					return false;
				break;
			case statePublished:
				if (this.state != stateAccepted)
					return false;
				break;
			default:
				return false;
		}
		// check reason
		if (state == stateDeleted || state == stateRejected) {
			if (reason.isEmpty()) {
				return false;
			}		
			this.reason = reason;
		}
		// create number
		if (state == statePublished) {
			setAutoPublicationNumber();
		}
		// change state
		this.state = state;
		return true;
	}
}