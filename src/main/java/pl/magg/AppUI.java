package pl.magg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import lombok.val;
import pl.magg.entity.Application;

/**
 * Application UI - for manual testing
 *
 */
public class AppUI {
	private Manager manager;

	public AppUI() {
		manager = new Manager();
	}

	private int readInputInt() {
		val toParse = readInputLine();
		int ret = Integer.parseInt(toParse);
		return ret;
	}

	private String readInputLine() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s;
		try {
			s = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
		if (s == null) {
			s = "";
		}
		return s;
	}

	private void addApplication() {
		System.out.println("--------------------------");
		System.out.println("Adding application");
		System.out.println("Give application name:");
		val name = readInputLine();
		System.out.println("Give application content:");
		val content = readInputLine();
		val app = manager.CreateApplication(name, content);
		System.out.println("Application created. ID = " + app.getId());
	}

	private int getState() {
		System.out.println("Choose state:");
		System.out.println("1. " + Application.getStateName(Application.stateCreated));
		System.out.println("2. " + Application.getStateName(Application.stateVerified));
		System.out.println("3. " + Application.getStateName(Application.stateAccepted));
		System.out.println("4. " + Application.getStateName(Application.statePublished));
		System.out.println("5. " + Application.getStateName(Application.stateDeleted));
		System.out.println("6. " + Application.getStateName(Application.stateRejected));
		System.out.println("0. No state");
		val decision = readInputLine();
		switch (decision) {
		case "1":
			return Application.stateCreated;
		case "2":
			return Application.stateVerified;
		case "3":
			return Application.stateAccepted;
		case "4":
			return Application.statePublished;
		case "5":
			return Application.stateDeleted;
		case "6":
			return Application.stateRejected;
		default:
			return 0;
		}
	}

	private void printList(String name, int state, Long id) {
		int startAt = 0, num;
		while (true) {
			val apps = manager.listApplications(name, state, id, startAt);
			System.out.println("List of results:");
			System.out.println("No | ID  |State        | Name");
			System.out.println("         |PublicationNo| Content[/Reason]");
			System.out.println("--------------------------------------------");
			int nr = startAt;
			for (val app : apps) {
				nr++;
				String line = String.format("%2d.|%5d|%12s |%s", nr, app.getId(),
						Application.getStateName(app.getState()), app.getName());
				System.out.println(line);
				System.out.println(String.format("         |%12s |%s",
						app.getPublicationNumber() == null ? "" : app.getPublicationNumber().toString(),
						app.getContent()));
				if (app.getReason() != null) {
					System.out.println(String.format("         |             |%s", app.getReason()));
				}
			}
			System.out.println("Decide what to do:");
			System.out.println("1. Modify application content");
			System.out.println("2. Modify application state");
			System.out.println("3. Show history for record");
			System.out.println("8. Show previous");
			System.out.println("9. Show next");
			System.out.println("0. Go back");
			val decision = readInputLine();
			switch (decision) {
			case "1":
				System.out.println("Give record no");
				num = readInputInt();
				if (num > 0) {
					num = num - startAt - 1;
					if (apps.size() > num && num >= 0) {
						val ob = apps.get(num);
						System.out.println("Object ID = " + ob.getId());
						System.out.println("Give new content");
						val content = readInputLine();
						if (ob.setContent(content)) {
							manager.updateApplication(ob);
							System.out.println("New content set");
						} else {
							System.out.println("New content not set!");
						}
					}
				}
				break;
			case "2":
				System.out.println("Give record no");
				num = readInputInt();
				if (num > 0) {
					num = num - startAt - 1;
					if (apps.size() > num && num >= 0) {
						val ob = apps.get(num);
						System.out.println("Object ID = " + ob.getId());
						val newState = getState();
						String reason = "";
						if (newState == Application.stateRejected || newState == Application.stateDeleted) {
							System.out.println("Please give a reason");
							reason = readInputLine();
						}
						if (ob.setState(newState, reason)) {
							manager.updateApplication(ob);
							System.out.println("New state set");
						} else {
							System.out.println("New state not set!");
						}
					}
				}
				break;
			case "3":
				System.out.println("Give record no");
				num = readInputInt();
				if (num > 0) {
					num = num - startAt - 1;
					if (apps.size() > num && num >= 0) {
						val ob = apps.get(num);
						val hList = manager.getHistory(ob.getId());
						System.out.println("History of object ID = " + ob.getId());
						for (val h : hList) {
							System.out.println(
									"State: " + Application.getStateName(h.getState()) + " at: " + h.getTimestamp());
						}
						System.out.println("Press enter to continue");
						val t = readInputLine();
					}
				}
				break;
			case "8":
				startAt -= 10;
				if (startAt < 0)
					startAt = 0;
				break;
			case "9":
				startAt += 10;
				break;
			case "0":
				return;
			}
		}
	}

	private void listApplications() {
		String name = "";
		int state = 0;
		String id = "";
		boolean doWhile = true;
		while (doWhile) {
			System.out.println("--------------------------");
			System.out.println("List applications:");
			System.out.println("1. Set name filter (current = " + name + ")");
			System.out.println("2. Set state filter (current = " + Application.getStateName(state) + ")");
			System.out.println("3. Set ID filter (current = " + id + ")");
			System.out.println("4. List applications");
			System.out.println("0. Back to menu");
			val decision = readInputLine();
			switch (decision) {
			case "1":
				System.out.println("Give name filter");
				name = readInputLine();
				break;
			case "2":
				state = getState();
				break;
			case "3":
				System.out.println("Give ID");
				id = readInputLine();
				break;
			case "4":
				printList(name, state, id.isEmpty() ? null : Long.parseLong(id));
				break;
			case "0":
				doWhile = false;

			}
		}

	}

	public void runUI() {
		boolean doLoop = true;
		while (doLoop) {
			System.out.println("===================================");
			System.out.println("Decide what to do:");
			System.out.println("1. Add Application");
			System.out.println("2. List Applications");
			System.out.println("0. Exit");
			String decision = readInputLine();
			switch (decision) {
			case "1":
				addApplication();
				break;
			case "2":
				listApplications();
				break;
			case "0":
				doLoop = false;
				break;
			}
		}
		System.out.println("Thank you for using this app.");
	}
}
